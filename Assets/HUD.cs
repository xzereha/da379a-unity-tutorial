﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HUD : MonoBehaviour
{
    [SerializeField] private Text m_pauseText;
    private bool m_gamePaused = false;

    private void Start()
    {
        m_pauseText.enabled = false;
    }

    public void Pause()
    {
        if (m_gamePaused = !m_gamePaused)
            PauseGame();
        else
            ResumeGame();

        m_pauseText.enabled = m_gamePaused;
    }

    public void ChangeScene()
    {
        SceneManager.LoadScene("hugoScene");
    }

    void PauseGame()
    {
        Time.timeScale = 0f;
    }

    void ResumeGame()
    {
        Time.timeScale = 1;
    }
}
