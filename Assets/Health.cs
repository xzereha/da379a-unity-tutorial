﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    [SerializeField] private Slider m_healthbar;
    [SerializeField] private float m_health = 5;
    private float m_maxHealth;

    private void Start()
    {
        m_maxHealth = m_health;
        UpdateHealthbar();
    }

    private void UpdateHealthbar()
    {
        m_healthbar.value = Percentage();
    }

    private float Percentage()
    {
        return m_health / m_maxHealth;
    }

    public void Damage(float damageAmount)
    {
        m_health -= Mathf.Abs(damageAmount);
        m_health = Mathf.Clamp(m_health, 0, m_maxHealth);
        Debug.Log("Damage done: " + damageAmount + ", Health left: " + m_health);
        UpdateHealthbar();
    }

    public void FillHealth()
    {
        m_health = m_maxHealth;
        UpdateHealthbar();
    }
}
