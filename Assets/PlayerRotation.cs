﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRotation : MonoBehaviour
{
    [SerializeField] float m_senistivity = 1;
    void Update()
    {
        transform.Rotate(transform.up, Input.GetAxis("Mouse X") * m_senistivity);
    }
}
