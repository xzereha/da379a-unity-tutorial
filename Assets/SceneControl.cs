﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneControl : MonoBehaviour
{
    [SerializeField] Light m_light;
    [SerializeField] GameObject m_spider;
    [SerializeField] Transform[] m_spawnLocations;
    private List<GameObject> m_spiders = new List<GameObject>();
    private bool m_night = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S) && m_spiders.Count < 300)
        {
            m_light.color = new Color(0.1f, 0.1f, 0.3f);
            spawnSpiders();
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            m_light.color = new Color(1f, 0.7196f, 0.3726415f);
            destroySpiders();
        }
    }

    void spawnSpiders()
    {
        int m_spiderAmount = Random.Range(0, 3);
        foreach (Transform t in m_spawnLocations)
        {
            for (int i = 0; i < m_spiderAmount; i++)
            {
                GameObject spider = Instantiate(m_spider, t);
                spider.transform.localScale *= Random.Range(0.5f, 1.5f);
                m_spiders.Add(spider);
            }
        }
    }

    void destroySpiders()
    {
        foreach (var spider in m_spiders){
            Destroy(spider);
        }

        m_spiders.Clear();
    }
}
