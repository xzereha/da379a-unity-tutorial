﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent), typeof(MeshRenderer))]
public class GoToPoint : MonoBehaviour
{
    public bool Selected { get { return m_selected; } set 
        { 
            m_selected = value; 
            if(m_selected)
            {
                m_mesh.material = m_selectedMaterial != null ? m_selectedMaterial : m_baseMaterial;
            }
            else
            {
                m_mesh.material = m_baseMaterial;
            }
        } 
    }
    [SerializeField] private Material m_selectedMaterial;
    private Material m_baseMaterial;
    private MeshRenderer m_mesh;
    private NavMeshAgent m_agent;
    private Vector3 m_target;
    private bool m_selected;

    private void Start()
    {
        m_mesh = GetComponent<MeshRenderer>();
        m_agent = GetComponent<NavMeshAgent>();
        m_selected = false;
        m_target = Vector3.zero;
        m_baseMaterial = m_mesh.material;
    }

    // Update is called once per frame
    private void Update()
    {
        if(m_selected && Input.GetMouseButtonDown(1))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(ray, out RaycastHit hit))
            {
                m_target = hit.point;
                m_agent.SetDestination(m_target);
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (m_selected && m_agent.isStopped == false)
        {
            Gizmos.color = Color.blue;
            foreach (var point in m_agent.path.corners)
            {
                Gizmos.DrawSphere(point, 0.25f);
            }
        }
    }
}
