﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class Hunt : MonoBehaviour
{
    private NavMeshAgent m_agent;
    private GameObject m_target;
    private float m_attackTime = 1f;
    private float m_updateTime = 0.2f;
    private float m_attackRange = 1.2f;
    private float m_damage = 1f;

    private void Start()
    {
        m_agent = GetComponent<NavMeshAgent>();
        m_target = GameObject.FindGameObjectWithTag("Player");
        StartCoroutine(UpdateSpider());
        StartCoroutine(UpdateAttack());
    }

    private void OnDrawGizmos()
    {
        if (m_agent.isStopped == false)
        {
            Gizmos.color = Color.red;
            foreach (var point in m_agent.path.corners)
            {
                Gizmos.DrawSphere(point, 0.25f);
            }
        }

        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, m_attackRange);
    }

    IEnumerator UpdateSpider()
    {
        while (true)
        {
            yield return new WaitForSeconds(m_updateTime);
            m_agent.SetDestination(m_target.transform.position);
        }
    }

    IEnumerator UpdateAttack()
    {
        while (true)
        {
            if ((m_target.transform.position - m_agent.transform.position).magnitude < m_attackRange)
            {
                m_target.GetComponent<Health>().Damage(m_damage);
                yield return new WaitForSeconds(m_attackTime);
            }
            else
            {
                yield return new WaitForSeconds(m_updateTime);
            }
        }
    }
}