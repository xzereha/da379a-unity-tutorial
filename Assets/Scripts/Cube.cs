﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            GetComponent<MeshRenderer>().material.SetColor("_BaseColor", Color.red);
            GetComponent<MeshRenderer>().material.SetColor("_Color", Color.red);
        }
    }
}
