﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField] GameObject m_door;
    private void OnTriggerEnter(Collider other)
    {
        m_door.SetActive(false);
        this.enabled = false;
    }
}
