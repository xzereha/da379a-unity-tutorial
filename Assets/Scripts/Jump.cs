﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    [SerializeField] private float m_jumpForce = 10;
    [SerializeField] private float m_gravity = 9.82f;
    private float y_velocity;
    private CharacterController m_controller;

    private void Start()
    {
        m_controller = GetComponent<CharacterController>();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if(m_controller.isGrounded)
                y_velocity = m_jumpForce;
        }
    }

    private void FixedUpdate()
    {
        m_controller.Move(new Vector3(0, y_velocity * Time.fixedDeltaTime));
        y_velocity -= m_gravity * Time.fixedDeltaTime;
    }
}
