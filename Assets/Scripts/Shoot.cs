﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    [SerializeField] private GameObject m_bulletPrefab;
    [SerializeField] private ParticleSystem m_laser;
    [SerializeField] private Transform m_barrel;
    [SerializeField] private float m_force = 2;
    [SerializeField] private float m_bulletVel = 5;
    private void Update()
    {
        if(Input.GetButtonDown("Fire1"))
        {
            m_laser.Play();
            Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            if(Physics.Raycast(ray, out RaycastHit hit, 100))
            {
                if(hit.transform.TryGetComponent<Rigidbody>(out Rigidbody body))
                {
                    body.AddForce(-hit.normal * m_force, ForceMode.Impulse);
                }
            }
        }
        if(Input.GetButtonDown("Fire2"))
        {
            var go = Instantiate(m_bulletPrefab, m_barrel.position, m_barrel.rotation);
            go.GetComponent<Rigidbody>().velocity = go.transform.forward * m_bulletVel;
            Destroy(go, 3);
        }
    }
}